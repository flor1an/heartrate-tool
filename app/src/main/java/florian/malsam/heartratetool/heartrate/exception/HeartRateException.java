package florian.malsam.heartratetool.heartrate.exception;

public class HeartRateException extends Exception {

    public HeartRateException(String s)
    {
        super(s);
    }
}
