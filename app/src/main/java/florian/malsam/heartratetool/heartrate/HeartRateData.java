package florian.malsam.heartratetool.heartrate;



public class HeartRateData {

    public HeartRateData(int bpm, long timestamp)
    {
        this.bpm = bpm;
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    private long timestamp;

    public int getBpm() {
        return bpm;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    private int bpm;


}
