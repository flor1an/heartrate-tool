package florian.malsam.heartratetool;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import florian.malsam.heartratetool.database.HeartRateToolContract;
import florian.malsam.heartratetool.database.HeartRateToolDBHelper;
import florian.malsam.heartratetool.heartrate.AntplusManager;
import florian.malsam.heartratetool.heartrate.HeartRateCallbacks;
import florian.malsam.heartratetool.heartrate.HeartRateData;
import florian.malsam.heartratetool.heartrate.HeartRateDataSet;
import florian.malsam.heartratetool.heartrate.HeartRateManager;
import florian.malsam.heartratetool.heartrate.HeartRateSensor;
import florian.malsam.heartratetool.heartrate.HeartrateEnum;
import florian.malsam.heartratetool.heartrate.BleManager;
import florian.malsam.heartratetool.heartrate.exception.HeartRateException;

public class MainActivity extends AppCompatActivity {

    public static int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    public static final String BASELINE_MEASUREMENT = "baseline";
    public static final String MEASUREMENT1 = "measurement1";
    public static final String MEASUREMENT2 = "measurement2";

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //chosen device type
    public HeartrateEnum DEVICE_TYPE = HeartrateEnum.DEVICE_TYPE_BLE;

    //tmp variable to safe type to DB
    String measurementType = null;

    HeartRateManager hsc = null;
    HeartRateSensor device = null;
    SharedPreferences sharedPref;
    HeartRateToolDBHelper dbHelper;
    boolean connecting = false;
    Button button_connect;
    Button button_export;
    Button button_start_measurement1;
    Button button_start_measurement2;
    Button button_baseline_measurement;
    Button button_stop_measurement;
    EditText mNameView;
    Context context;
    Activity activity;

    HeartRateCallbacks callbacks;

    ListView listView;
    private DeviceListAdapter mDevicesListAdapter = null;


    //reference for button which counts to zero
    Button counterButton;
    boolean measurement_started = false;
    int counterBaseline;
    boolean running = true;

    @Override
    protected void onStop() {
        super.onStop();
        if (hsc != null)
            hsc.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        context = this;
        setContentView(R.layout.activity_main);

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 1);

        if (Build.VERSION.SDK_INT < 24) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }


            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //ANSCHALTEN FALLS OFF
                Intent enableGPSIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(enableGPSIntent, 2);
            }

        }


        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }

        initUserDB();

        callbacks = new HeartRateCallbacks.Null() {
            @Override
            public void connectionStateChanged(HeartRateSensor s, HeartRateManager.CONNECTION state) {
                if (state == HeartRateManager.CONNECTION.CONNECTED)
                    handleDeviceConnected(s);
                else if (state == HeartRateManager.CONNECTION.DISCONNECTED)
                    handleDeviceDisonnected();
            }

            @Override
            public void measurementDone(HeartRateDataSet data) {
                running = false;

                insertDataToDB(data);
            }

            @Override
            public void deviceFound(final HeartRateSensor device) {
                handleFoundDevice(device);
            }

            @Override
            public void newHeartrateData(HeartRateData d) {
                handleNewHeartrateData(d);
            }

            @Override
            public void secondDone(int counter) {
                Log.d("MainActivity", "counter: " + counter);
            }


        };


        initStuff();
        disableStartButtons();
        button_stop_measurement.setEnabled(false);
        button_connect.setEnabled(true);


    }

    protected void onStart() {
        super.onStart();

        try {
            if (DEVICE_TYPE == HeartrateEnum.DEVICE_TYPE_BLE) {
                hsc = new BleManager(this, callbacks);
            } else {
                hsc = new AntplusManager(this, callbacks);
            }
        } catch (HeartRateException e) {
            hsc = null;
        }


    }


    void handleNewHeartrateData(HeartRateData d) {

        if (measurement_started) {


            measurement_started = false;
            new Thread(new Runnable() {

                public void run() {
                    while (counterBaseline > 0 && running) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        counterButton.post(new Runnable() {

                            public void run() {
                                counterButton.setText("" + counterBaseline);

                            }

                        });
                        counterBaseline--;
                    }

                    runOnUiThread(new Runnable() {
                        public void run() {

                            enableStartButtons();
                        }
                    });

                }

            }).start();
        }
    }

    public void initStuff() {
        sharedPref = context.getSharedPreferences(
                getString(R.string.shared_userdata), Context.MODE_PRIVATE);

        mNameView = (EditText) findViewById(R.id.name);
        String name = sharedPref.getString(getString(R.string.saved_name), "");
        mNameView.setText(name);
        mNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString(getString(R.string.saved_name), mNameView.getText().toString());
                editor.commit();
            }
        });

        button_connect = (Button) findViewById(R.id.button_connect);
        button_connect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setContentView(R.layout.scanning);
                setContentView(R.layout.scanning);

                mDevicesListAdapter = new DeviceListAdapter(activity);
                listView = (ListView) findViewById(android.R.id.list);
                listView.setAdapter(mDevicesListAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        setContentView(R.layout.activity_main);

                        initStuff();

                        final HeartRateSensor device = mDevicesListAdapter.getDevice(position);
                        if (device == null) return;


                        try {
                            hsc.connect(device);
                        } catch (Exception e) {

                        }

                    }
                });

                if (hsc != null)
                    hsc.startScanning();


            }
        });

        button_export = (Button) findViewById(R.id.button_export);
        button_export.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                export_clicked();
            }
        });


        button_baseline_measurement = (Button) findViewById(R.id.button_baseline_measurement);
        button_baseline_measurement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                measurement_clicked(BASELINE_MEASUREMENT);
            }
        });


        button_stop_measurement = (Button) findViewById(R.id.button_stop_measurement);
        button_stop_measurement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stop_measurement();
            }


        });


        button_start_measurement1 = (Button) findViewById(R.id.button_measurement1);
        button_start_measurement1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                measurement_clicked(MEASUREMENT1);
            }
        });


        button_start_measurement2 = (Button) findViewById(R.id.button_measurement2);
        button_start_measurement2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                measurement_clicked(MEASUREMENT2);
            }
        });
    }

    private void handleFoundDevice(final HeartRateSensor device) {
        // adding to the UI have to happen in UI thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDevicesListAdapter.addDevice(device);
                mDevicesListAdapter.notifyDataSetChanged();
            }
        });
        Log.d("MainActivity", "found device: " + device.getName());
    }

    public void showToast(final String toast) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), toast, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void export_clicked() {


        File f = new File("/data/data/florian.malsam.heartratetool/databases/HeartRateTool.db");
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {

            fis = new FileInputStream(f);
            fos = new FileOutputStream("/mnt/sdcard/db_dump.db");
            while (true) {
                int i = fis.read();
                if (i != -1) {
                    fos.write(i);
                } else {
                    break;
                }
            }
            fos.flush();
            Toast.makeText(this, "DB dump OK", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "DB dump ERROR", Toast.LENGTH_LONG).show();
        } finally {
            try {
                fos.close();
                fis.close();
            } catch (IOException ioe) {
            }
        }

        File filelocation = new File("/mnt/sdcard/db_dump.db");
        Uri path = Uri.fromFile(filelocation);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
// set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {"malsam.florian@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
// the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, path);
// the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }


    void measurement_clicked(String m) {

        counterBaseline = 60;
        if (MEASUREMENT1 == m) {
            counterButton = button_start_measurement1;
        } else if (MEASUREMENT2 == m) {
            counterButton = button_start_measurement2;
        } else if (BASELINE_MEASUREMENT == m) {
            counterButton = button_baseline_measurement;
            counterBaseline = 120;
        }


        measurement_started = true;

        if (hsc != null) {
            hsc.clear();
        }
        running = true;
        disableStartButtons();
        measurementType = m;

        if (hsc != null)
            try {
                hsc.startMeasurement(counterBaseline);
            } catch (HeartRateException e) {

            }


    }


    private void initUserDB() {


        dbHelper = new HeartRateToolDBHelper(this);


    }


    private void stop_measurement() {

        HeartRateDataSet data = hsc.stopMeasurement();

        running = false;

        insertDataToDB(data);

        enableStartButtons();

    }


    private void insertDataToDB(HeartRateDataSet data) {
        if (measurementType != null) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_USER, sharedPref.getString(getString(R.string.saved_name), ""));
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_SCENARIO, measurementType);
            String deviceName = "";
            if (device != null)
                deviceName = device.getName();
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_DEVICE, deviceName);
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_START, data.getStart());
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_END, data.getEnd());
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_HIGHEST, data.getMaxRate());
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_LOWEST, data.getMinRate());
            values.put(HeartRateToolContract.Measurements.COLUMN_NAME_AVG, data.getAvgRate());

            long newRowId = db.insert(HeartRateToolContract.Measurements.TABLE_NAME, null, values);

            for (HeartRateData d : data.getHeartRateValues()
                    ) {
                ContentValues values2 = new ContentValues();
                values2.put(HeartRateToolContract.MeasurementData.COLUMN_MEASUREMENTDATA_MEASUREMENTID, newRowId);
                values2.put(HeartRateToolContract.MeasurementData.COLUMN_MEASUREMENTDATA_TIMESTAMP, d.getTimestamp());
                values2.put(HeartRateToolContract.MeasurementData.COLUMN_MEASUREMENTDATA_HEARTRATE, d.getBpm());

                long newRowId2 = db.insert(HeartRateToolContract.MeasurementData.TABLE_NAME, null, values2);
            }

            showToast("measurement finished. max: " + data.getMaxRate() + ", avg: " + data.getAvgRate() + ", dauer: " + (data.getEnd() / 1000 - data.getStart() / 1000));


        }

        measurementType = null;


    }


    private void handleDeviceConnected(HeartRateSensor s) {


        device = s;

        final TextView connected_textview = (TextView) findViewById(R.id.connected_text);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //stuff that updates ui
                String connected = "Connected";
                if (device != null)
                    connected += " to " + device.getName();
                connected_textview.setText(connected);

                final Button button = (Button) findViewById(R.id.button_status);
                button.setBackgroundResource(R.drawable.connected_shape);

                enableStartButtons();
            }
        });

        connecting = true;
        hsc.stopScanning();


    }

    private void handleDeviceDisonnected() {


        final TextView connected_textview = (TextView) findViewById(R.id.connected_text);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //stuff that updates ui
                connected_textview.setText("Not connected");

                final Button button = (Button) findViewById(R.id.button_status);
                if (button != null)
                    button.setBackgroundResource(R.drawable.not_connected_shape);

                initButtons();
            }
        });

        connecting = false;


    }

    private void initButtons() {

        button_start_measurement1.setEnabled(false);
        button_start_measurement1.setText("Start measurement 1");
        button_start_measurement2.setEnabled(false);
        button_start_measurement2.setText("Start measurement 2");
        button_baseline_measurement.setEnabled(false);
        button_baseline_measurement.setText("Start baseline measurement");
        button_stop_measurement.setEnabled(false);
        button_connect.setEnabled(true);
        running = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {


        }
    }

    void enableStartButtons() {
        button_start_measurement1.setEnabled(true);
        button_start_measurement1.setText("Start measurement 1");
        button_start_measurement2.setEnabled(true);
        button_start_measurement2.setText("Start measurement 2");
        button_baseline_measurement.setEnabled(true);
        button_baseline_measurement.setText("Start baseline measurement");
        button_stop_measurement.setEnabled(false);
        button_connect.setEnabled(true);
    }

    void disableStartButtons() {
        button_start_measurement1.setEnabled(false);
        button_start_measurement2.setEnabled(false);
        button_baseline_measurement.setEnabled(false);
        button_stop_measurement.setEnabled(true);
        button_connect.setEnabled(false);
    }
}
