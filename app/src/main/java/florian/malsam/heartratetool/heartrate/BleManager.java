package florian.malsam.heartratetool.heartrate;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.util.Log;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import florian.malsam.heartratetool.heartrate.alternatescanrecord.AlternateScanRecord;
import florian.malsam.heartratetool.heartrate.exception.HeartRateException;

/**
 * bluetooth low energy manager
 * handles BLE scan, connect, measurements
 */

public class BleManager extends HeartRateManager {

    private static String HEARTRATE_SERVICE_UUID = "0000180d-0000-1000-8000-00805f9b34fb";
    private static String HEARTRATE_MEASUREMENT_CHARACTERISTIC_UUID = "00002a37-0000-1000-8000-00805f9b34fb";
    private static String CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR_UUID = "00002902-0000-1000-8000-00805f9b34fb";

    private Activity mParent = null;

    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothGatt mBluetoothGatt = null;
    private BluetoothGattDescriptor mBluetoothGattDescriptor = null;
    private ScanCallback mDeviceFoundCallback = null;
    private BluetoothAdapter.LeScanCallback mLeScanCallback = null;


    public BleManager(Activity parent, HeartRateCallbacks callbacks) throws HeartRateException {
        super(callbacks);
        mParent = parent;
        initialize();
    }

    /**
     *  get bluetooth manager and adapter, check if BLE supported
     * @throws HeartRateException if error occurs getting manager oder adapter, or if BLE is not supported
     */
    public void initialize() throws HeartRateException {

        if (!mParent.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            throw new HeartRateException("Hardware does not support BLE");
        }


        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) mParent.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                throw new HeartRateException("Could not get Bluetooth Manager");
            }
        }

        if (mBluetoothAdapter == null) mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            throw new HeartRateException("Could not get Bluetooth Adapter");
        } else {
            //enable bluetooth without user interaction
            //mBluetoothAdapter.enable();
        }
    }


    /**
     * method to start scan for BLE devices, calls deviceFound(..) callback
     */
    @Override
    public void startScanning() {

        deviceMap.clear();
        if(mBluetoothGatt!=null)
            mBluetoothGatt.close();

        /**
         * different method in old API
         */
        if (Build.VERSION.SDK_INT < 21) {
            mBluetoothAdapter.startLeScan(mLeScanCallback =
                    new BluetoothAdapter.LeScanCallback() {
                        @Override
                        public void onLeScan(final BluetoothDevice device, int rssi,
                                             byte[] scanRecord) {
                            if(device != null)
                                Log.d("BleManagerOld", "device found: "+device.getName());

                            BleSensor mdv = new BleSensor(device);

                            AlternateScanRecord a = AlternateScanRecord.parseFromBytes(scanRecord);

                            //if found device provides heartrate service, add to found devices
                            if (mdv != null && a != null && a.getServiceUuids().contains(new ParcelUuid(UUID.fromString(HEARTRATE_SERVICE_UUID)))) {
                                callBacks.deviceFound(mdv);
                                if (!deviceMap.containsKey(mdv.getAdress()))
                                    deviceMap.put(mdv.getAdress(), mdv);
                            }
                        }
                    });
        } else {
            ScanSettings.Builder builder = new ScanSettings.Builder();
            //continuous scan
            builder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
            if (mBluetoothAdapter != null && builder != null)
                mBluetoothAdapter.getBluetoothLeScanner().startScan(null, builder.build(), mDeviceFoundCallback = new ScanCallback() {
                    public void onScanResult(int callbackType, ScanResult result) {
                        if (Build.VERSION.SDK_INT > 20) {
                            BleSensor mdv = null;
                            if(result != null)
                            {
                                mdv = new BleSensor(result.getDevice());
                                Log.d("BleSensor", "found device: " + mdv.getName());
                                //if found device provides heartrate service, add to found devices
                                if (mdv != null && result.getScanRecord() != null && result.getScanRecord().getServiceUuids() != null && result.getScanRecord().getServiceUuids().contains(new ParcelUuid(UUID.fromString(HEARTRATE_SERVICE_UUID)))) {
                                    callBacks.deviceFound(mdv);
                                    if (!deviceMap.containsKey(mdv.getAdress()))
                                        deviceMap.put(mdv.getAdress(), mdv);
                                }
                            }
                        }
                    }

                    /**
                     * Callback when batch results are delivered.
                     *
                     * @param results List of scan results that are previously scanned.
                     */
                    public void onBatchScanResults(List<ScanResult> results) {
                    }

                    /**
                     * Callback when scan could not be started.
                     *
                     * @param errorCode Error code (one of SCAN_FAILED_*) for scan failure.
                     */
                    public void onScanFailed(int errorCode) {

                    }

                });
        }
    }

    @Override
    public void stopScanning() {
        if (Build.VERSION.SDK_INT < 21) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        } else {
            if (mDeviceFoundCallback != null)
                mBluetoothAdapter.getBluetoothLeScanner().stopScan(mDeviceFoundCallback);

        }

    }


    /**
     * connect to a sensor, given the sensor object
     * @param s the sensor object found in the scan
     * @throws HeartRateException if connect fails
     */
    @Override
    public void connect(HeartRateSensor s) throws HeartRateException {

        if (mBluetoothGatt != null)
            mBluetoothGatt.disconnect();
        if (mBluetoothGatt != null && mBluetoothGatt.getDevice().getAddress().equals(s.getAdress())) {
            Log.d("BleManager", "reconnect");
            // just reconnect
            if (!mBluetoothGatt.connect()) {
                throw new HeartRateException("Reconnect failed");
            }
        }

        Log.d("BleManager", "normal connect");
        if(((BleSensor)s).getBluetoothObject() != null)
        {
            ((BleSensor)s).getBluetoothObject().connectGatt(mParent, false, btleGattCallback);
        }
        else{
            BluetoothDevice btDevice = mBluetoothAdapter.getRemoteDevice(s.getAdress());
            mBluetoothGatt = btDevice.connectGatt(mParent, false, btleGattCallback);
        }

        if (mBluetoothGatt == null)
            throw new HeartRateException("Connect failed");
    }


    @Override
    public void close() {

        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
    }


    /**
     * connection to device must exist.
     * @throws HeartRateException
     */
    @Override
    public void startMeasurement() throws HeartRateException {
        running = true;
        if (mBluetoothGatt != null) {
            List<BluetoothGattService> services = mBluetoothGatt.getServices();
            for (BluetoothGattService service : services) {
                List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                if (service.getUuid().toString().equals(HEARTRATE_SERVICE_UUID)) {
                    for (BluetoothGattCharacteristic c : characteristics) {

                        Log.d("BleManager", "found characteristic "+c.getUuid().toString());
                        if (c.getUuid().toString().equals(HEARTRATE_MEASUREMENT_CHARACTERISTIC_UUID)) {
                            List<BluetoothGattDescriptor> descriptors;
                            if ((descriptors = c.getDescriptors()) != null && descriptors.size() > 0) {
                                BluetoothGattDescriptor descriptor = descriptors.get(0);
                                //Client Characteristic Configuration
                                if (descriptor.getUuid().toString().equals(CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR_UUID)) {

                                    //find descriptor UUID that matches Client Characteristic Configuration (0x2902)
                                    // and then call setValue on that descriptor
                                    boolean success = mBluetoothGatt.setCharacteristicNotification(c, true);
                                    if (!success) {
                                        throw new HeartRateException("Setting proper notification status for characteristic failed!");
                                    }
                                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                                    mBluetoothGatt.writeDescriptor(descriptor);
                                }
                            }else
                                throw new HeartRateException("GATT Descriptor not available");
                        }
                    }
                }
            }
        } else
            throw new HeartRateException("Device seems to be not connected");
    }


    @Override
    public HeartRateDataSet stopMeasurement() {
        running = false;
        if (mBluetoothGatt != null && mBluetoothGattDescriptor != null) {
            mBluetoothGattDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(mBluetoothGattDescriptor);
        }
        return getHeartRateDataSet();
    }

    /**
     * connect via adress
     * @param adress adress of sensor
     * @throws HeartRateException
     */
    @Override
    public void connect(String adress) throws HeartRateException {

        if (adress != null && adress.length() > 0) {
            BleSensor sensor = new BleSensor(adress);
            connect(sensor);

        } else
            throw new HeartRateException("Can not connect, address invalid or null");
    }


    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {

        //new heartrate data arrived
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

            if (timer_started) {
                timer_started = false;
                new Thread(new Runnable() {

                    public void run() {
                        while (counter > 0 && running) {
                            try {
                                Thread.sleep(1000);


                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            counter--;
                            callBacks.secondDone(counter);
                        }
                        //nur wenn timer abgelaufen
                        if (running) {
                            callBacks.measurementDone(stopMeasurement());
                            running = false;
                        }


                    }

                }).start();
            }

            if (running) {

                //int type = (characteristic.getValue()[0] & 0x01);

                Date d = new Date();
                if(characteristic != null)
                {
                    int heartRate = Math.abs(characteristic.getValue()[1]);

                    Log.d("BleManager", "new heartrate: "+heartRate);

                    HeartRateData tmp = new HeartRateData(heartRate, d.getTime());
                    if (callBacks != null)
                        callBacks.newHeartrateData(tmp);
                    getHeartRateValues().add(tmp);
                }

            }


        }


        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {

            if (gatt != null && deviceMap != null) {
                BleSensor bleDevice = (BleSensor) deviceMap.get(gatt.getDevice().getAddress());
                if (bleDevice == null)
                    bleDevice = new BleSensor(gatt.getDevice());
                if (newState == BluetoothGatt.STATE_CONNECTED) {
                    callBacks.connectionStateChanged(bleDevice, CONNECTION.CONNECTED);
                    Log.d("BleManager", "Device connected " + bleDevice.getName());
                    mBluetoothGatt = gatt;
                    gatt.discoverServices();
                } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                    running = false;

                    callBacks.connectionStateChanged(bleDevice, CONNECTION.DISCONNECTED);

                    clear();

                    Log.d("BleManager", "Device disconnected " + bleDevice.getName());
                } else if (newState == BluetoothGatt.STATE_CONNECTING)
                    callBacks.connectionStateChanged(bleDevice, CONNECTION.CONNECTING);
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {

            Log.d("BleManager", "Services discovered");
            mBluetoothGatt = gatt;
        }
    };

}
