package florian.malsam.heartratetool.heartrate;

import android.app.Activity;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.AsyncScanController;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

import florian.malsam.heartratetool.heartrate.exception.HeartRateException;

/**
 * ANT+ manager
 * handles ANT+ scan, connect, measurements
 */

public class AntplusManager extends HeartRateManager {


    private Activity mParent = null;

    protected PccReleaseHandle<AntPlusHeartRatePcc> releaseHandle = null;
    AsyncScanController<AntPlusHeartRatePcc> hrScanCtrl;

    /**
     * handle for device
     */
    AntPlusHeartRatePcc heartRatePcc;


    public AntplusManager(Activity parent, HeartRateCallbacks callbacks) {
        super(callbacks);
        mParent = parent;
    }

    @Override
    public void startScanning() {

        deviceMap.clear();
        if(heartRatePcc!=null)
            heartRatePcc.releaseAccess();
        requestAccessToPcc();
    }


    /**
     * not necessary in ANT+
     * scan is paused after connect
     */
    @Override
    public void stopScanning() {
    }

    @Override
    public void initialize() throws HeartRateException {

    }

    @Override
    public void connect(HeartRateSensor s) {

        AntplusSensor as = (AntplusSensor) s;
        callBacks.connectionStateChanged(s, CONNECTION.CONNECTING);
        requestConnectToResult((AsyncScanController.AsyncScanResultDeviceInfo) as.getRealDevice());

    }


    @Override
    public void close() {

        if (hrScanCtrl != null) {
            hrScanCtrl.closeScanController();
            hrScanCtrl = null;
        }

    }

    @Override
    public void startMeasurement() {
        running = true;
        subscribeToHrEvents();
    }


    @Override
    public HeartRateDataSet stopMeasurement() {
        running = false;
        heartRatePcc.subscribeHeartRateDataEvent(null);
        return getHeartRateDataSet();
    }

    @Override
    public void connect(final String s) throws HeartRateException {

        final HeartRateSensor sensor;
        final int address;
        try {
            address = Integer.valueOf(s);
        } catch (Exception e) {
            throw new HeartRateException("Can not connect, address invalid or null");
        }


        PccReleaseHandle pccReleaseHandle = AntPlusHeartRatePcc.requestAccess(mParent, address, 0, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc>() {
            @Override
            public void onResultReceived(AntPlusHeartRatePcc result,
                                         RequestAccessResult resultCode, DeviceState initialDeviceState) {

                if (result != null) {
                    AntplusSensor sensor = new AntplusSensor(result.getDeviceName());
                    sensor.setAdress(String.valueOf(result.getAntDeviceNumber()));

                    if (resultCode == RequestAccessResult.SEARCH_TIMEOUT) {
                        callBacks.connectionStateChanged(sensor, CONNECTION.DISCONNECTED);
                    } else {
                        callBacks.connectionStateChanged(sensor, CONNECTION.CONNECTED);
                        hrScanCtrl = null;
                        heartRatePcc = result;
                    }
                }

            }
        }, base_IDeviceStateChangeReceiver);

    }

    /**
     * start scanning
     */
    protected void requestAccessToPcc() {

        if (hrScanCtrl != null) {
            hrScanCtrl.closeScanController();
            hrScanCtrl = null;
        }

        hrScanCtrl = AntPlusHeartRatePcc.requestAsyncScanController(mParent, 0,
                new AsyncScanController.IAsyncScanResultReceiver() {
                    @Override
                    public void onSearchStopped(RequestAccessResult reasonStopped) {

                    }

                    @Override
                    public void onSearchResult(final AsyncScanController.AsyncScanResultDeviceInfo deviceFound) {
                        Log.d("AntplusManager", "searchresult adress: " + deviceFound.getAntDeviceNumber());
                        AntplusSensor gd = new AntplusSensor(deviceFound);
                        gd.setName(deviceFound.getDeviceDisplayName());
                        callBacks.deviceFound(gd);
                        if (!deviceMap.containsKey(gd.getAdress()))
                            deviceMap.put(gd.getAdress(), gd);
                        else
                            Log.d("AntplusManager", "already contains device");
                    }
                });


    }

    /**
     * connect method
     * @param asyncScanResultDeviceInfo the device to connect to
     */
    protected void requestConnectToResult(final AsyncScanController.AsyncScanResultDeviceInfo asyncScanResultDeviceInfo) {


        //Inform the user we are connecting
        releaseHandle = hrScanCtrl.requestDeviceAccess(asyncScanResultDeviceInfo,
                new AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc>() {
                    @Override
                    public void onResultReceived(AntPlusHeartRatePcc result,
                                                 RequestAccessResult resultCode, DeviceState initialDeviceState) {
                        if (resultCode == RequestAccessResult.SEARCH_TIMEOUT) {
                            callBacks.connectionStateChanged(new AntplusSensor(asyncScanResultDeviceInfo), CONNECTION.DISCONNECTED);
                        } else {
                            callBacks.connectionStateChanged(new AntplusSensor(asyncScanResultDeviceInfo), CONNECTION.CONNECTED);
                           // base_IPluginAccessResultReceiver.onResultReceived(result, resultCode, initialDeviceState);
                            hrScanCtrl = null;
                            heartRatePcc = result;
                        }
                    }
                }, base_IDeviceStateChangeReceiver);

    }

    /**
     * subscribe heartratedata to start measurement
     */
    public void subscribeToHrEvents() {

        heartRatePcc.subscribeHeartRateDataEvent(new AntPlusHeartRatePcc.IHeartRateDataReceiver() {
            @Override
            public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                           final int computedHeartRate, final long heartBeatCount,
                                           final BigDecimal heartBeatEventTime, final AntPlusHeartRatePcc.DataState dataState) {
                // Mark heart rate with asterisk if zero detected
                final String textHeartRate = String.valueOf(computedHeartRate)
                        + ((AntPlusHeartRatePcc.DataState.ZERO_DETECTED.equals(dataState)) ? "*" : "");


                if (timer_started) {
                    timer_started = false;
                    new Thread(new Runnable() {

                        public void run() {
                            while (counter > 0 && running) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                counter--;
                                callBacks.secondDone(counter);
                            }
                            //timer done
                            if (running) {
                                callBacks.measurementDone(stopMeasurement());
                                running = false;
                            }


                        }

                    }).start();
                }

                if (running) {
                    Date d = new Date();
                    int heartRate = 0;
                    try {
                        heartRate = Math.abs(Integer.parseInt(textHeartRate));
                    } catch (Exception e) {

                    }


                    Log.d("AntplusManager",  "new heartrate: "+heartRate);

                    HeartRateData tmp = new HeartRateData(heartRate, d.getTime());
                    if (callBacks != null)
                        callBacks.newHeartrateData(tmp);
                    getHeartRateValues().add(tmp);
                }
            }
        });


    }

    /**
     * receives notification if device disconnected
     */
    protected AntPluginPcc.IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver =
            new AntPluginPcc.IDeviceStateChangeReceiver() {
                @Override
                public void onDeviceStateChange(final DeviceState newDeviceState) {

                    if(heartRatePcc != null)
                    {
                        if(deviceMap.containsKey(heartRatePcc.getAntDeviceNumber()))

                            if(DeviceState.CLOSED == newDeviceState)
                                callBacks.connectionStateChanged(deviceMap.get(heartRatePcc.getAntDeviceNumber()), CONNECTION.DISCONNECTED);
                            else if(DeviceState.DEAD == newDeviceState)
                                callBacks.connectionStateChanged(deviceMap.get(heartRatePcc.getAntDeviceNumber()), CONNECTION.DISCONNECTED);
                    }


                }
            };


}
